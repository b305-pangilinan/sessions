import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ActivateCourse({ course }) {
  const activateCourse = (courseId) => {
    fetch(`http://localhost:4000/courses/${courseId}/activate`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: 'success',
            title: 'Course Activated!',
            text: 'Course Activating Successfully',
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonColor: '#0275d8',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Course Activating Failed!',
            text: 'Please try again',
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: 'Try Again!',
            confirmButtonColor: '#d9534f',
          });
        }
      });
  };
  return (
    <>
      <Button variant="success" onClick={() => activateCourse(course)}>
        Activate
      </Button>
    </>
  );
}
