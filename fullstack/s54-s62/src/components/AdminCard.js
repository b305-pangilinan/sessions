import { Table } from 'react-bootstrap';
import ArchiveCourse from './ArchiveCourse';
import EditCourse from './EditCourse';
export default function AdminCard({ coursesData }) {
  const { _id, name, description, price, isActive } = coursesData;
  return (
    <>
      <Table>
        <tr key={_id}>
          <td>{_id}</td>
          <td>{name}</td>
          <td>{description}</td>
          <td>{price}</td>
          <td className={isActive ? 'text-success' : 'text-danger'}>
            {isActive ? 'Available' : 'Unavailable'}
          </td>
          <td>
            <EditCourse course={_id} />
          </td>
          <td>
            {isActive ? (
              <ArchiveCourse course={_id} />
            ) : (
              <button className="btn btn-success">Activate</button>
            )}
          </td>
        </tr>
      </Table>
    </>
  );
}
