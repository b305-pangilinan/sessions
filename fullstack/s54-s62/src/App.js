import './App.css';
import AppNavbar from './components/AppNavbar';
import Courses from './pages/Courses';
import Home from './pages/Home';

//State Components
import { useEffect, useState } from 'react';

// Route Components
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';

//Bootstrap Components
import { Container } from 'react-bootstrap';

//Page Components
import AdminView from './pages/AdminView';
import CourseView from './pages/CourseView';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Page404 from './pages/Page404';
import Profile from './pages/Profile';
import Register from './pages/Register';
// Provider Component
import { UserProvider } from './context/UserContext';

function App() {
  // Gets token from ocal storage and assigned the token to user State
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
    // token: localStorage.getItem('token')
  });

  // unsetUser is used to remove user from user state when user logs out
  const unsetUser = () => {
    localStorage.clear();
  };

  // Checking user info properly upon login and localStorage if cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  return (
    // The user state, setUser (setter function) and unsetUser function is provided in the UserContext to be shared to other pages/components.
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />}></Route>
            <Route path="/courses" element={<Courses />}></Route>
            <Route path="/profile" element={<Profile />}></Route>
            <Route path="/register" element={<Register />}></Route>
            <Route path="/courses/:courseId" element={<CourseView />}></Route>
            <Route path="/adminView" element={<AdminView />}></Route>
            <Route path="/login" element={<Login />}></Route>
            <Route path="/logout" element={<Logout />}></Route>
            <Route path="*" element={<Page404 />}></Route>
          </Routes>
        </Container>
      </Router>
    </UserProvider>

    // <>
    //   <AppNavbar />
    //   <Container>
    //     {/* <Home />
    //     <Courses />
    //     <Register /> */}
    //     <Login />
    //   </Container>
    // </>
  );
}

export default App;
