import { useContext, useEffect, useState } from 'react';
import CourseCard from '../components/CourseCard';
import UserContext from '../context/UserContext';
import AdminView from './AdminView';
// import coursesData from '../data/coursesData';

export default function Courses() {
  const { user } = useContext(UserContext);

  const [courses, setCourses] = useState([]);

  useEffect(() => {
    const link = user.isAdmin ? '/courses/all/' : '/courses/';
    fetch(`${process.env.REACT_APP_API_URL}${link}`)
      .then((res) => res.json())
      .then((data) => {
        // set State of courses
        // setCourses(
        //   user.isAdmin
        //     ? data
        //     : data.map((course) => {
        //         return <CourseCard key={course._id} courseProp={course} />;
        //       })
        // );
        setCourses(
          user.isAdmin
            ? data
            : data.map((course) => {
                return <CourseCard key={course._id} courseProp={course} />;
              })
        );
        // setCourses(
        //   data.map((course) => {
        //     return <CourseCard key={course._id} courseProp={course} />;
        //   })
        // );
      });
  });
  // const courses = coursesData.map((course) => {
  //   return <CourseCard key={course.id} courseProp={course} />;
  // });
  return user.isAdmin ? (
    <>
      <AdminView key={courses._id} coursesData={courses} />
    </>
  ) : (
    <>{courses}</>
  );
}
