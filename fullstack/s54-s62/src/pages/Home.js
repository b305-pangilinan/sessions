import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
  const bannerText = {
    id: 1,
    title: 'Zuitt Coding Bootcamp',
    message: 'Opportunities for everyone, everywhere',
    text: 'Enroll Now!',
    link: '/',
  };

  return (
    <>
      <Banner key={bannerText.title} bannerProp={bannerText} />;
      <Highlights />
    </>
  );
}
