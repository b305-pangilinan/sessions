import { useContext, useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

// Context Component
import userContext from '../context/UserContext';

export default function Login() {
  // Context Hooks
  const { user, setUser } = useContext(userContext);

  // State Hooks
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function loginUser(e) {
    e.preventDefault();
    fetch('http://localhost:4001/users/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);

          // function for retrieving user details
          retrieveUserDetails(data.access);

          // SweetAlert
          Swal.fire({
            icon: 'success',
            title: 'Login Successful!',
            text: 'Welcome to Zuitt',
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonColor: '#0275d8',
          });
          // setUser({ access: localStorage.getItem('token'), id: data.userId });
          // alert('Login Successfully!');
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Authentication Failed!',
            text: 'Wrong E-mail or Password',
            allowOutsideClick: false,
            showConfirmButton: true,
            confirmButtonText: 'Try Again!',
            confirmButtonColor: '#d9534f',
          });
        }
      });
    setEmail('');
    setPassword('');
  }

  const retrieveUserDetails = (token) => {
    fetch('http://localhost:4001/users/profile', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => loginUser(e)}>
      <h1 className="my-5 text-center">Login</h1>
      <Form.Group>
        <Form.Label>Email: </Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter Email"
          required
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        />
      </Form.Group>
      <Form.Group>
        <Form.Label>Password:</Form.Label>
        <Form.Control
          type="password"
          placeholder="Enter Password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
          required
        />
      </Form.Group>
      <Form.Group className="mt-3">
        <Button variant="primary" type="submit" disabled={isActive === false}>
          Submit
        </Button>
      </Form.Group>
    </Form>
  );
}
