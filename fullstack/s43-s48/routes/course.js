const express = require('express');
const courseController = require('../controllers/course.js');

const auth = require('../auth.js');

// destructing of verify and verifyAdmin

const { verify, verifyAdmin } = auth;

const router = express.Router();

// Create a course POST
router.post('/', verify, verifyAdmin, courseController.addCourse);

// Get all courses
router.get('/all', courseController.getAllCourses);

// Get all "active" course
router.get('/', courseController.getAllActive);

// Get 1 specific course using its ID
router.get('/:courseId', courseController.getCourse);

// Updating a Course (Admin Only)
router.put('/:courseId', verify, verifyAdmin, courseController.updateCourse);

// Archive a course
router.put('/:courseId/archive', verify, verifyAdmin, courseController.archiveCourse);

// Activating a course
router.put('/:courseId/activate', verify, verifyAdmin, courseController.activateCourse);

router.post('/archives', verify, verifyAdmin, courseController.archives);

module.exports = router;
