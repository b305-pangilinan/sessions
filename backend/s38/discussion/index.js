let http = require('http');

let port = 4000;

let app = http.createServer((request, response) => {
  if (request.url.toLowerCase() === '/items' && request.method === 'GET') {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.end('Data retrieved from the database.');
  } else if (request.url.toLowerCase() === '/items' && request.method === 'POST') {
    response.writeHead(200, { 'Content-Type': 'application/json' });
    response.end('Data to be sent to the database.');
  } else {
    response.writeHead(404, { 'Content-Type': 'text/plain' });
    response.end('Page not found.');
  }
});

app.listen(port, () => console.log(`Server is running at localhost:${port}`));
