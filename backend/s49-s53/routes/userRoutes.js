// Dependencies
const express = require('express');
const auth = require('../auth.js');

// Routing Component
const router = express.Router();

//Controller
const userController = require('../controllers/userController.js');

// De-Structure Auth
const { verify, verifyAdmin } = auth;

// User Registration End Point
router.post('/register', userController.registerUser);

// User Login
router.post('/login', userController.userLogin);

// Export Router
module.exports = router;
