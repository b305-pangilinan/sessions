// Dependencies
const express = require('express');

// Routing Component
const router = express.Router();

// Export Router
module.exports = router;
