// Dependencies
const jwt = require('jsonwebtoken');
const secret = 'ECOMAPI';

// Create Token
module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(data, secret, {});
};

// Retreive token from the request header
module.exports.verify = (req, res, next) => {
  // Auth Type is Bearer Token
  console.log(req.headers.authorization);

  let token = req.headers.authorization;

  if (typeof token === 'undefined') {
    return res.send({ auth: 'Failed, No Token!' });
  } else {
    console.log(token);
    token = token.slice(7, token.length);

    console.log(token);

    jwt.verify(token, secret, function (error, decodedToken) {
      if (error) {
        return res.send({
          auth: 'Failed',
          message: error.message,
        });
      } else {
        console.log(decodedToken);
        req.user = decodedToken;

        // Will let the controller to proceed next
        next();
      }
    });
  }
};

// Verify User Level
module.exports.verifyAdmin = (req, res, next) => {
  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: 'Failed!',
      message: 'Access Forbidden',
    });
  }
};
