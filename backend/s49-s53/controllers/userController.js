// Dependencies
const Users = require('../models/User.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');

// User Registration
const registerUser = async (req, res) => {
  const hashPassword = await bcrypt.hash(req.body.password, 10);
  const newUser = new Users({
    email: req.body.email,
    password: hashPassword,
  });

  return newUser.save().then((user, err) => {
    if (err) {
      res.status(500).send('Internal Server Error');
    } else {
      res.status(201).send('User Created!');
    }
  });
};

// User Login
const userLogin = async (req, res) => {
  return Users.findOne({ email: req.body.email })
    .then((user) => {
      if (user === null) {
        return res.status(401).send('Invalid Credentials! Please Try Again!');
      } else {
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);
        if (isPasswordCorrect) {
          return res.send({ access: auth.createAccessToken(user) });
        } else {
          return res.send(false);
        }
      }
    })
    .catch((err) => err);
};

// Exports
module.exports = {
  registerUser,
  userLogin,
};
