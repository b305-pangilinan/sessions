const express = require('express');
const mongoose = require('mongoose');
const taskRoute = require('./routes/taskRoute.js');

// Setup + Middlewares
const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// default endpoint
app.use('/tasks', taskRoute);

// DB Connection
mongoose.connect(
  'mongodb+srv://admin:admin123@cluster0.w7dkija.mongodb.net/B305-to-do?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', () => {
  console.log('Connected to Cloud Database');
});

// Server Listener
if (require.main === module) {
  app.listen(port, () => {
    console.log(`Server is running at port ${port}`);
  });
}
