//Dummy Data for S36 Discussion
db.fruits.insertMany([
  {
    name: 'Apple',
    color: 'Red',
    stock: 20,
    price: 40,
    supplier_id: 1,
    onSale: true,
    origin: ['Philippines', 'US'],
  },

  {
    name: 'Banana',
    color: 'Yellow',
    stock: 15,
    price: 20,
    supplier_id: 2,
    onSale: true,
    origin: ['Philippines', 'Ecuador'],
  },

  {
    name: 'Kiwi',
    color: 'Green',
    stock: 25,
    price: 50,
    supplier_id: 1,
    onSale: true,
    origin: ['US', 'China'],
  },

  {
    name: 'Mango',
    color: 'Yellow',
    stock: 10,
    price: 120,
    supplier_id: 2,
    onSale: false,
    origin: ['Philippines', 'India'],
  },
]);

// Aggregate Methods

/*

SYNTAX:

- {$match: {field: value}}
- $group - groups by field elements altogether

db.collectionName.aggregate([
  {$match: {fieldA: valueA}},
  {$group: {_id: '$fieldB', {result: {$sum: '$fieldC'}}},},
])

$sum - to get the total value of a field

*/

db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $group: {
      _id: '$supplier_id',
      total: { $sum: '$stock' },
    },
  },
]);

// Field Projection with Aggregation

// $poject - To include and exclude data from an aggregation

db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $group: {
      _id: '$supplier_id',
      total: { $sum: '$stock' },
    },
  },
  {
    $project: {
      _id: 0,
      total: 1,
    },
  },
]);

// Sorting Aggregated Results
// $sort = -1 Descending / 1 Ascending
db.fruits.aggregate([
  {
    $match: { onSale: true },
  },
  {
    $group: {
      _id: '$supplier_id',
      total: { $sum: '$stock' },
    },
  },
  {
    $project: {
      _id: 1,
      total: 1,
    },
  },
  {
    $sort: { _id: 1 },
  },
]);

// Aggregating results based on array fields

// {$unwind: field}

db.fruits.aggregate([{ $unwind: '$origin' }]);

// Display fruit documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
  {
    $unwind: '$origin',
  },
  {
    $group: {
      _id: '$origin',
      kinds: { $sum: 1 },
      fruits: { $push: '$name' },
    },
  },
]);
