const http = require('http');

// Creates a variable "port" to the port number

const port = 4001;

const app = http.createServer((req, res) => {
  if (req.url == '/greetings') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Hello Again!');
  } else if (req.url == '/homepage') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('This is the Homepage');
  } else {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('404 HTTP Not Found');
  }
});

app.listen(port);

console.log(`Server is now accessible at localhost: ${port}`);
