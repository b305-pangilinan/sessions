const http = require('http');

// Creates a variable "port" to the port number

const port = 3000;

const app = http.createServer((req, res) => {
  if (req.url == '/login') {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('Welcome to the login page.');
  } else {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end("I'm sorry the page you are looking for cannot be found.");
  }
});

app.listen(port);

//Do not modify
module.exports = { app };
