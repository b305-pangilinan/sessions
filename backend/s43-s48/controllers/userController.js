// Dependencies
const Users = require('../models/Users.js');
const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Check Existing E-mail
const checkEmailExist = (reqBody) => {
  console.log(reqBody);
  return Users.find({ email: reqBody.email }).then((result) => {
    console.log(result);
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

const registerUser = (reqBody) => {
  const newUser = new Users({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
  });
  return newUser
    .save()
    .then((newUser, err) => {
      if (err) {
        return 'error';
      } else {
        return 'Registered Successfully';
      }
    })
    .catch((err) => {
      if (err.code) {
        return 'Duplicate E-mail address';
      }
    });
};

// User Authentication
const loginUsers = (req, res) => {
  return Users.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
        return res.send(false);
      } else {
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
        console.log(isPasswordCorrect);
        if (isPasswordCorrect) {
          return res.send({ access: auth.createAccessToken(result) });
        } else {
          return res.send(false);
        }
      }
    })
    .catch((err) => res.send(err));
};

// Get User Details by ID
const getProfile = (req, res) => {
  return Users.findById(req.user.id)
    .then((result) => {
      result.password = '';
      return res.send(result);
    })
    .catch((error) => res.send(error));
};

// Enroll Users
const enrollUser = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send('You cannot do this action.');
  }
  const courseEnroll = (courseId) => {
    return Course.findById(courseId).then((course) => {
      const userCollect = {
        userId: req.user.id,
      };
      course.enrollees.push(userCollect);
      const enrollNow = course
        .save()
        .then((course) => true)
        .catch((err) => res.send(err));
      if (enrollNow) {
        return course;
      } else {
        return false;
      }
    });
  };
  const courseEncode = await courseEnroll(req.body.courseId);
  const newEnrollment = (userId, courseInfo) => {
    return Users.findById(userId).then((user) => {
      const courseCollect = {
        courseId: courseInfo._id,
        courseName: courseInfo.name,
        courseDescription: courseInfo.description,
        coursePrice: courseInfo.price,
      };
      const search = user.enrollments.filter((enrollment) => enrollment.courseId === req.body.courseId);
      if (search.length > 0) {
        return 'You are already enrolled in this course';
      } else {
        user.enrollments.push(courseCollect);

        const userEnrollment = user
          .save()
          .then((user) => true)
          .catch((err) => res.send(err));
        if (userEnrollment) {
          return true;
        } else {
          return false;
        }
      }
    });
  };
  if (courseEncode === false) {
    return res.send('Something went wrong');
  } else {
    const enrollment = await newEnrollment(req.user.id, courseEncode);
    if (enrollment === true) {
      res.send('User Enrolled Successfully');
    } else if (enrollment === false) {
      res.send('Something went wrong');
    } else {
      res.send(enrollment);
    }
  }
};

// Other Models
// const enrollUser = (req, res) => {
//   if (req.user.isAdmin) {
//     return res.send('You cannot do this action.');
//   }
//   return Course.findById(req.body.courseId)
//     .then((course) => {
//       const newEnrollment = {
//         courseId: course._id,
//         courseName: course.name,
//         courseDescription: course.description,
//         coursePrice: course.price,
//       };
//       const newEnrollee = {
//         userId: req.user.id,
//       };
//       course.enrollees.push(newEnrollee);
//       const enrollNow = course
//         .save()
//         .then((course) => true)
//         .catch((err) => res.send(err));
//       if (enrollNow) {
//         const userCourse = Users.findById({ _id: req.user.id }).then((user) => {
//           const search = user.enrollments.filter((enrollment) => enrollment.courseId === req.body.courseId);
//           if (search.length > 0) {
//             res.send('You are already enrolled to this course');
//           } else {
//             user.enrollments.push(newEnrollment);

//             const enrollment = user
//               .save()
//               .then((user) => true)
//               .catch((err) => res.send(err));
//             if (enrollment) {
//               return res.send('You are now Enrolled to this course!');
//             } else {
//               return res.send('Something went wrong.');
//             }
//           }
//         });
//       }
//     })
//     .catch((err) => res.send(err));
// };

// const enrollUser = async (req, res) => {
//   if (req.user.isAdmin) {
//     return res.send('You cannot do this action.');
//   }

//   const isUserUpdated = await Users.findById(req.user.id).then((user) => {
//     const newEnrollment = {
//       courseId: req.body.courseId,
//       courseName: req.body.courseName,
//       courseDescription: req.body.courseDescription,
//       coursePrice: req.body.coursePrice,
//     };

//     user.enrollments.push(newEnrollment);

//     return user
//       .save()
//       .then((user) => true)
//       .catch((err) => res.send(err));
//   });
//   if (isUserUpdated !== true) {
//     return res.send({ message: isUserUpdated });
//   }

//   const isCourseUpdated = await Course.findById(req.body.courseId).then((course) => {
//     const newEnrollee = {
//       userId: req.user.id,
//     };

//     course.enrollees.push(newEnrollee);

//     return course
//       .save()
//       .then((course) => true)
//       .catch((err) => res.send(err));
//   });
//   if (isCourseUpdated !== true) {
//     return res.send({ message: isCourseUpdated });
//   }

//   if (isUserUpdated && isCourseUpdated) {
//     return res.send('You are now enrolled to this course. Thank you.');
//   } else {
//     return res.send('Something went wrong. Please try again!');
//   }
// };

// Activate User
const activateUser = (req, res) => {
  const activateUser = {
    isActive: true,
  };

  Users.findById(req.params.userId).then((result, err) => {
    if (!result.isActive) {
      return Users.findByIdAndUpdate(req.params.userId, activateUser).then((active, err) => {
        if (err) {
          return res.send(err);
        } else {
          return res.send(`User: ${active.firstName} ${active.lastName} is now active.`);
        }
      });
    } else {
      return res.send('This user is already active');
    }
  });
};

// Deactivate User
const deactivateUser = (req, res) => {
  const deactivateUser = {
    isActive: false,
  };

  Users.findById(req.params.userId).then((result, err) => {
    if (result.isActive) {
      return Users.findByIdAndUpdate(req.params.userId, deactivateUser).then((deactive, err) => {
        if (err) {
          return res.send(err);
        } else {
          return res.send(`User: ${deactive.firstName} ${deactive.lastName} is now deactivated.`);
        }
      });
    } else {
      return res.send('This user is already deactivated');
    }
  });
};

// Get Enrollments
const getEnrollments = (req, res) => {
  Users.findById(req.user.id).then((result, err) => {
    if (err) {
      return res.send(err);
    } else {
      if (result.enrollments.length === 0) {
        return res.send('You are not enrolled to any courses');
      } else {
        return res.send(result.enrollments);
      }
    }
  });
};

// Reset Password
const resetPassword = async (req, res) => {
  try {
    const newPassword = req.body.newPassword;
    const userId = req.user.id;

    // Hashing Password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    await Users.findByIdAndUpdate(userId, { password: hashedPassword });

    res.status(200).json({ message: 'Password has been reset successfully' });
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }
};

//Exports
try {
  module.exports = {
    checkEmailExist: checkEmailExist !== undefined ? checkEmailExist : null,
    registerUser: registerUser !== undefined ? registerUser : null,
    loginUsers: loginUsers !== undefined ? loginUsers : null,
    getProfile: getProfile !== undefined ? getProfile : null,
    enrollUser: enrollUser !== undefined ? enrollUser : null,
    activateUser: activateUser !== undefined ? activateUser : null,
    deactivateUser: deactivateUser !== undefined ? deactivateUser : null,
    getEnrollments: getEnrollments !== undefined ? getEnrollments : null,
    resetPassword: resetPassword !== undefined ? resetPassword : null,
  };
} catch (err) {
  console.log(err);
}
