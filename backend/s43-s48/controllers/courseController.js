// Dependencies
const Courses = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Add Course
const addCourse = (req, res) => {
  const newCourse = new Courses({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });
  Courses.find({ name: req.body.name }).then((result) => {
    if (result.length > 0) {
      return res.send('Duplicate Course.');
    } else {
      return newCourse
        .save()
        .then((newCourse, err) => {
          if (err) {
            return res.send(err);
          } else {
            return res.send(true);
          }
        })
        .catch((err) => res.send(err));
    }
  });
};

const getAllCourses = (req, res) => {
  return Courses.find({})
    .then((result) => {
      if (result.length === 0) {
        return res.send('There is no record.');
      } else {
        return res.send(result);
      }
    })
    .catch((err) => {
      return res.send(err);
    });
};

const getAllActive = (req, res) => {
  return Courses.find({ isActive: true }).then((result) => {
    if (result.length === 0) {
      return res.send('There is no for active courses.');
    } else {
      return res.send(result);
    }
  });
};

const getCourse = (req, res) => {
  return Courses.findById(req.params.courseId)
    .then((result) => {
      // return res.send(result);
      if (result.length === 0 || result.length === null || result === undefined) {
        return res.send('There is no record.');
      } else {
        if (result.isActive === false) {
          return res.send('The course your trying to access is not available.');
        } else {
          return res.send(result);
        }
      }
    })
    .catch((err) => {
      return res.send('Invalid Course ID.');
    });
};

const updateCourse = (req, res) => {
  const updateCourse = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  };
  return Courses.findByIdAndUpdate(req.params.courseId, updateCourse).then((result, err) => {
    if (err) {
      return res.send(err);
    } else {
      return res.send(true);
    }
  });
};

const archiveCourse = (req, res) => {
  const archiveCourse = {
    isActive: false,
  };
  Courses.findById(req.params.courseId)
    .then((result, err) => {
      if (result.isActive) {
        return Courses.findByIdAndUpdate(req.params.courseId, archiveCourse).then((update, err) => {
          if (err) {
            return res.send(err);
          } else {
            return res.send(`The course ${result.name} is now archived.`);
          }
        });
      } else {
        return res.send('This course is already inactive');
      }
    })
    .catch((err) => res.send(err));
};

const activateCourse = (req, res) => {
  const activateCourse = {
    isActive: true,
  };
  Courses.findById(req.params.courseId)
    .then((result, err) => {
      if (!result.isActive) {
        return Courses.findByIdAndUpdate({ id: req.params.courseId }, activateCourse).then((result, err) => {
          if (err) {
            return res.send(err);
          } else {
            return res.send(`The course ${result.name} is now active.`);
          }
        });
      } else {
        return res.send('This course is already active');
      }
    })
    .catch((err) => res.send(err));
};

// Exports
try {
  module.exports = {
    addCourse: addCourse !== undefined ? addCourse : null,
    getAllCourses: getAllCourses !== undefined ? getAllCourses : null,
    getAllActive: getAllActive !== undefined ? getAllActive : null,
    getCourse: getCourse !== undefined ? getCourse : null,
    updateCourse: updateCourse !== undefined ? updateCourse : null,
    archiveCourse: archiveCourse !== undefined ? archiveCourse : null,
    activateCourse: activateCourse !== undefined ? activateCourse : null,
  };
} catch (err) {
  console.log(err);
}
