// Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

// Enviroment Variables
require('dotenv').config();

//Access Routes
const userRoutes = require('./routes/usersRoutes.js');
const courseRoutes = require('./routes/courseRoutes.js');

// Enviroment Setup
const port = 4000;

// Server Setup
const app = express();

// Routes
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

// Database Connection
mongoose.connect(process.env.dbURL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// DB Connection Check
const dbConnection = mongoose.connection;
dbConnection.on('error', console.error.bind(console, 'connection error:'));
dbConnection.once('open', () => {
  console.log('Connected to Atlas Cloud Database');
});

// Back-end Routes
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

// Server Gateway Response
if (require.main === module) {
  app.listen(process.env.PORT || port, () => {
    console.log(`API is now online on port: ${process.env.PORT || port}`);
  });
}

// Export App
module.exports = app;
