// Dependencies
const express = require('express');

// Routing Component
const router = express.Router();
const userController = require('../controllers/userController.js');
const auth = require('../auth.js');

// Destructure from auth
let { verify, verifyAdmin } = auth;

// Check E-mail Routes
router.post('/checkEmail', (req, res) => {
  userController.checkEmailExist(req.body).then((resultFromController) => res.send(resultFromController));
});

// User Registration Routes
router.post('/register', (req, res) => {
  userController.registerUser(req.body).then((resultFromController) => res.send(resultFromController));
});

// User Login
router.post('/login', userController.loginUsers);

// User Details Retreival
// router.post('/details', (req, res) => {
//   userController.getProfile(req.body.id).then((resultFromController) => res.send(resultFromController));
// });

router.get('/details', verify, userController.getProfile);

// Enroll user to a course
router.post('/enroll', verify, userController.enrollUser);

// Activate User
router.post('/:userId/activate', verify, verifyAdmin, userController.activateUser);

// Deactivate User
router.post('/:userId/deactivate', verify, verifyAdmin, userController.deactivateUser);

// Get Enrollments
router.get('/getEnrollments', verify, userController.getEnrollments);

// Reset Passwords
router.put('/reset-password', verify, userController.resetPassword);

// Exports
module.exports = router;
