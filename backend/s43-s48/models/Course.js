// Dependencies and Modules
const mongoose = require('mongoose');

// Schema
const courseSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Course is Required'],
  },
  description: {
    type: String,
    required: [true, 'Description is Required'],
  },
  price: {
    type: Number,
    required: [true, 'Price is Required'],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  enrollees: [
    {
      userId: {
        type: String,
        required: [true, 'User Id is Required'],
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: 'Enrolled',
      },
    },
  ],
});

// Model Exports
module.exports = mongoose.model('Course', courseSchema);
