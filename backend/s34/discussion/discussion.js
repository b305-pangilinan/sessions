// CRUD Operations (MongoDB)

/*
C - Create
R - Read
U - Update
D - Delete
*/
// INSERT

/*
Syntax:

db.users.insertOne(
{
  object
}
)
*/

B305.users.insertOne({
  firstName: 'Test',
  lastName: 'Test',
  age: 0,
  contact: {
    phone: '0000',
    email: 'test@gmail.com',
  },
  courses: [],
  department: 'none',
});

db.users.insertMany([
  {
    firstName: 'Stephen',
    lastName: 'Hawking',
    age: 76,
    contact: {
      phone: '09123456789',
      email: 'stephenhawking@mail.com',
    },
    courses: ['Front-End', 'Back-End', 'Full Stack'],
    department: 'IT',
  },
  {
    firstName: 'Neil',
    lastName: 'Armstrong',
    age: 82,
    contact: {
      phone: '09123456789',
      email: 'neilarmstrong@email.com',
    },
    courses: ['CSS', 'JS', 'Python'],
    department: 'none',
  },
]);

db.users.insertOne({
  firstName: 'Test',
  lastName: 'Test',
  age: 0,
  contact: {
    phone: '0000',
    email: 'test@gmail.com',
  },
  courses: [],
  department: 'none',
});

db.users.updateOne(
  { firstName: 'Test' },
  {
    $set: {
      firstName: 'Bill',
      lastName: 'Gates',
      age: 65,
      contact: {
        phone: '09123456789',
        email: 'billgates@gmail.com',
      },
      courses: ['PHP', 'Laravel', 'HTML'],
      department: 'Operations',
      status: 'active',
    },
  }
);

db.users.updateMany(
  {
    status: '',
  },
  {
    $set: {
      status: 'active',
    },
  }
);

db.users.insert({
  firstName: 'Test',
  lastName: 'Test',
  age: 0,
  contact: {
    phone: '0000',
    email: 'none@gmail.com',
  },
  courses: [],
  department: 'none',
  status: 'inactive',
});

db.users.deleteOne({
  firstName: 'Test',
});
