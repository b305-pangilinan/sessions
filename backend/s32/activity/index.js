/* 1. Try these activities to practice your skills in array manipulation/methods<br>

    a. Create an array-practice folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Do these activities inside the index.js file:<br>

    1. Write a simple JS function to join all elements of the following array into a string:
          - color = ["Blue","Red","Yellow","Orange","Green","Purple"]
    2. Create a function that will add a planet at the beginning of the array:
          - planet = ["Venus","Mars","Earth"]
    3. Create an array of your favorite singers' names. Create a function that will remove the last name in the array. Then, replace the deleted name with other two names.

    d. Send a screenshot of your work's console in the Batch 297 Thread for the Reading List Activities<br> */

function color() {
  let color = ['Blue', 'Red', 'Yellow', 'Orange', 'Green', 'Purple'];
  console.log(color.join());
}
color();

function planet() {
  let planet = ['Venus', 'Mars', 'Earth'];
  console.log(planet);
  planet.unshift('Mercury');
  console.log(planet);
}
planet();

function singers() {
  let singers = ['Dan Reynolds', 'Mike Shinoda', 'Chester Bennington'];
  console.log(singers);
  // singers.pop();
  // console.log(singers);
  singers.splice(2, 1, 'John Rzeznik', 'Adam Levine');
  // singers.push('John Rzeznik', 'Adam Levine');
  console.log(singers);
}

singers();

/* 2. Create a student grading system (Selection Control and ES6 Updates)<br>
    a. Create an grading-function folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Create a student grading system using an arrow function that will receive the grade from a prompt. The grade categories are as follows:<br>
         - Novice (80-below)
         - Apprentice (81-90)
         - Master (91-100)

    d. Send a screenshot of your work's console in the Batch 297 Thread for the Reading List Activities<br> */

const gradingSystem = (grade) => {
  if (grade <= 80) {
    console.log('Novice');
  } else if (grade >= 81 && grade <= 90) {
    console.log('Apprentice');
  } else if (grade >= 91 && grade <= 100) {
    console.log('Master');
  }
};

let grade = prompt('Enter your grade: ');
gradingSystem(grade);

/* 3. Create an odd-even checker (For Loop)<br>
    a. Create an odd-even-checker folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Create an odd-even checker function that will check which numbers from 1-50 are odd and which are even.<br>

    Sample output in the console: <br>
        1 - The number 1 is odd <br>
        2 - The number 2 is even <br>
        3 - The number 3 is odd <br>

    d. Send a screenshot of your work's console in the Batch 297 Thread for the Reading List Activities<br> */

function oddEvenChecker() {
  for (let i = 1; i <= 50; i++) {
    if (i % 2 == 0) {
      console.log(`The number ${i} is even`);
    } else {
      console.log(`The number ${i} is odd`);
    }
  }
}
oddEvenChecker();

/* 4. Create a word counter (DOM Manipulation)<br>

    a. Create a word-counter folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Follow the step by step guide in creating a Word Counter App using the resource given<br>
    d. Send a screenshot of your work in the Batch 297 Thread for the Reading List Activities<br> */

const textArea = document.querySelector('#text-area');
const countButton = document.querySelector('#count-button');

countButton.addEventListener('click', function countWords() {
  let text = textArea.value;
  let wordCount = text.split(' ').length;
  let spaceCount = text.split(' ').length - 1;
  let charCount = text.length - spaceCount;
  document.querySelector('#word-count').innerHTML = `Word Count: ${wordCount} <br>
  Character Count without spaces: ${charCount} <br>
  Space Count: ${spaceCount}`;
});
/* 5. Create a Toggle Password Visibility Feature (DOM Manipulation)<br>

    a. Create a visibility-toggle folder in the 230714 folder<br>
    b. Create your index.html and index.js inside of it<br>
    c. Follow the step by step guide in creating the Toggle Password Visibility Feature using the resource given<br>
    d. Send a screenshot of your work in the Batch 297 Thread for the Reading List Activities<br></br> */

const togglePassword = document.querySelector('#toggle-password');
const password = document.querySelector('#password');

togglePassword.addEventListener('click', function toggle() {
  if (password.type === 'password') {
    password.type = 'text';
  } else {
    password.type = 'password';
  }
});
