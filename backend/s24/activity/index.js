// console.log("Hello World");

//Objective 1
//Add code here
//Note: function name is numberLooper

function numberLooper(num1) {
  console.log(`The number you provided is ${num1}`);
  for (let i = num1; i >= 0; i--) {
    if (i % 10 === 0 && i > 50) {
      console.log('The number is divisible by 10. Skipping the number.');
    } else if (i % 5 === 0 && i > 50) {
      console.log(i);
    }
    if (i <= 50) {
      console.log('The current value is at 50. Terminating the loop.');
      break;
    }
  }
}
let loopCount = 100;
numberLooper(loopCount);

//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for (let i = 0; i < string.length; i++) {
  if (
    string[i].toLocaleLowerCase() !== 'a' &&
    string[i].toLocaleLowerCase() !== 'e' &&
    string[i].toLocaleLowerCase() !== 'i' &&
    string[i].toLocaleLowerCase() !== 'o' &&
    string[i].toLocaleLowerCase() !== 'u'
  ) {
    filteredString += string[i];
  }
}
console.log(filteredString);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
    numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null,
  };
} catch (err) {}
