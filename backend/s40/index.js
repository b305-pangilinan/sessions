const express = require('express');

const app = express();
const port = 4000;

// Middlewares
app.use(express.json());
// Allows your app to read data from forms
app.use(express.urlencoded({ extended: true }));
// accepts all data types

// [SECTION] Routes

app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.get('/about', (req, res) => {
  res.send('About');
});

// route expects to receive a POST method at the URI "/hello"
app.post('/about', (req, res) => {
  res.send(`Hello there, ${req.body.firstName} ${req.body.lastName}!`);
  res.end();
});

// POST route to register user
let users = [];

app.post('/signup', (req, res) => {
  let existingUser = users.some((user) => user.username === req.body.username);

  if (existingUser) {
    res.send(`User ${req.body.username} is already registered!`);
  } else {
    if (req.body.username !== '' && req.body.password !== '') {
      users.push(req.body);
      res.send(`${req.body.username} Registered!`);
      res.end();
    } else {
      res.send('Please fill out all fields');
      res.end();
    }
  }
});

// Change Password - PUT method
app.put('/change-password', (req, res) => {
  let message;
  for (let i = 0; i < users.length; i++) {
    if (users[i].username === req.body.username) {
      users[i].password = req.body.password;
      message = `User ${req.body.username}'s password has been updated!`;
    } else if (req.body.username === '') {
      message = 'No Username Indicated';
    } else {
      message = 'There is No Registered User!';
    }
    res.send(message);
    res.end();
  }
});

if (require.main === module) {
  app.listen(port, () => {
    console.log(`Server running on port ${port}`);
  });
}
